package com.chekushka.controller;

import com.chekushka.view.AppView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;

public class StreamsController {
    private static Logger logger = LogManager.getLogger(AppView.class);

    public List<Integer> streamTaskFirstWay() {
        List<Integer> list = new ArrayList<>();

        Collections.addAll(list, 1, 3, 7, 5, 2, 5, 6, 9);
        logger.info("Created list:\n" + list);
        logger.info("Min:" + list.stream()
                .min(Comparator.comparing(i -> i)));
        logger.info("Max:" + list.stream()
                .max(Comparator.comparing(i -> i)));
        OptionalDouble avgOptional = list.stream()
                .mapToInt(i -> i)
                .average();
        logger.info("Avg:" + avgOptional);
        logger.info("Sum:" + list.stream()
                .mapToInt(i -> i)
                .sum());

        double avg = avgOptional.getAsDouble();

        return list.stream()
                .filter(i -> i > avg)
                .collect(Collectors.toList());
    }

    public void streamTaskSecondWay() {
        List<Integer> list = new ArrayList<>();

        Collections.addAll(list, 1, 3, 7, 5, 2, 5, 6, 9);
        logger.info(list.stream()
                .mapToInt((x) -> x)
                .summaryStatistics());
    }

    public List<String> wordsStream(List<String> wordList){
        wordList.stream()
                .flatMap(i -> Arrays.stream(i.split(" ")))
                .map(String::toLowerCase)
                .collect(Collectors.groupingBy(i -> i, Collectors.counting()))
                .forEach((k, v) -> logger.info(k +" - "+ v));

        wordList.stream()
                .map(c->c.split(""))
                .distinct()
                .collect(Collectors.groupingBy(i -> i, Collectors.counting()))
                .forEach((k, v) -> logger.info(Arrays.toString(k) +" - "+ v));

        return wordList.stream()
                .map(String::toLowerCase)
                .distinct()
                .collect(Collectors.toList());
    }
}
