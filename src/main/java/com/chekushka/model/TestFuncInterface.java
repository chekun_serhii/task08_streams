package com.chekushka.model;

@FunctionalInterface
public interface TestFuncInterface {
    int testFunc(int first, int second , int third);

}
