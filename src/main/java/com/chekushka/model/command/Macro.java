package com.chekushka.model.command;

import java.util.ArrayList;
import java.util.List;

public class Macro {
    private final List<Command> commands;

    public Macro() {
        this.commands = new ArrayList<>();
    }

    public void record(Command command) {
        commands.add(command);
    }

    public void run() {
        for (Command command : commands) {
            command.perform();
        }
    }
}


