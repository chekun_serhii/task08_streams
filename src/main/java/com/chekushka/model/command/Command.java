package com.chekushka.model.command;

public interface Command {
        void perform();
}
