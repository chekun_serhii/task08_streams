package com.chekushka.model.command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class CommandImpl implements Command {
    private Logger logger = LogManager.getLogger(CommandImpl.class);
    private String name;

    public static void print(){
        System.out.println("command success." );
    }

    public CommandImpl(String name) {
        this.name = name;
    }

    @Override
    public void perform() {
        print();
    }
}
