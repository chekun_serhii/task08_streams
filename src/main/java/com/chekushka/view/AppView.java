package com.chekushka.view;

import com.chekushka.controller.StreamsController;
import com.chekushka.model.command.CommandImpl;
import com.chekushka.model.TestFuncInterface;
import com.chekushka.model.command.Macro;
import com.chekushka.view.entries.MenuEntry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AppView {
    private static Logger logger1 = LogManager.getLogger(AppView.class);
    private static Scanner input = new Scanner(System.in);
    private static StreamsController controller = new StreamsController();

    public final void show() {
        Menu menu = new Menu();
        menu.addEntry(new MenuEntry("First task.") {
            @Override
            public void run() {
                logger1.info("The first lambda:");
                TestFuncInterface firstLambda = (first, second, third) -> {
                    if (Integer.max(first, second) == first) {
                        if (Integer.max(first, third) == first) return first;
                        else return third;
                    } else {
                        if (Integer.max(second, third) == second) return second;
                        else return third;
                    }
                };
                logger1.info("The second lambda:");
                TestFuncInterface secondLambda = (first, second, third) -> (first + second + third) / 3;
            }
        });
        menu.addEntry(new MenuEntry("Second task.") {
            @Override
            public void run() {
                String commandName;
                logger1.info("Enter the command name:");
                String name = input.next();
                Macro newMacro = new Macro();
                CommandImpl newCommand = new CommandImpl(name);
                logger1.info("1. As lambda function"
                        + "2. As method reference"
                        + "3. as anonymous class"
                        + "4. as object of command class"
                        + "Select the method:");
                int choice = input.nextInt();
                if (choice == 1) {
                    newMacro.record(() -> CommandImpl.print());
                    newMacro.run();
                } else if (choice == 2) {
                    newMacro.record(CommandImpl::print);
                    newMacro.run();
                } else if (choice == 3) {
                    new CommandImpl(name).perform();
                } else if (choice == 4) {
                    CommandImpl.print();
                }
            }
        });
        menu.addEntry(new MenuEntry("Third task.") {
            @Override
            public void run() {
                logger1.info(new StreamsController()::streamTaskFirstWay);
            }
        });
        menu.addEntry(new MenuEntry("Fourth task") {
            @Override
            public void run() {
                List<String> list = new ArrayList<>();
                String s;
                logger1.info("Enter some text:");
                while (true) {
                    s = input.next();
                    if (s.isEmpty()) {
                        break;
                    }
                    list.add(s);
                }
                logger1.info(controller.wordsStream(list));
            }
        });
        menu.run();
    }
}
